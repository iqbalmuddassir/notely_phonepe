package muddassir.com.notely.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import muddassir.com.notely.models.Note;
import muddassir.com.notely.viewHolders.NoteItemVH;
import muddassir.com.notely.views.NoteItemView;

/**
 * @author muddassir on 11/17/17.
 */

public class NoteAdapter extends RecyclerView.Adapter<NoteItemVH> {
    private List<Note> notes;
    private Context context;
    private NoteItemVH.OnItemClickListener onItemClickListener;

    public NoteAdapter(Context context) {
        this.context = context;
    }

    @Override
    public NoteItemVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = new NoteItemView(context);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                , ViewGroup.LayoutParams.WRAP_CONTENT));
        return new NoteItemVH(view);
    }

    @Override
    public void onBindViewHolder(NoteItemVH holder, int position) {
        holder.setNote(notes.get(position));
        holder.setOnItemClickListener(onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return notes == null ? 0 : notes.size();
    }

    /**
     * Set the item click listener
     *
     * @param onItemClickListener to be used
     */
    public void setOnItemClickListener(NoteItemVH.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    /**
     * it returns the list of notes it is holding
     *
     * @return note list
     */
    public List<Note> getNotes() {
        return notes;
    }

    /**
     * Set the notes from external source
     *
     * @param notes list of notes
     */
    public void setNotes(List<Note> notes) {
        this.notes = notes;
        Collections.sort(this.notes, new Comparator<Note>() {
            @Override
            public int compare(Note o1, Note o2) {
                return o1.getDate().getTime() > o2.getDate().getTime() ? -1 : 0;
            }
        });
        notifyDataSetChanged();
    }

    /**
     * this method helps extracting an item available at the position specified
     *
     * @param position of the item
     * @return Note object
     */
    public Note getItem(int position) {
        return notes.get(position);
    }

    /**
     * set the note item value at the position specified
     *
     * @param position of the item
     * @param note     object to be updated with
     */
    public void set(int position, Note note) {
        notes.set(position, note);
        Collections.sort(this.notes, new Comparator<Note>() {
            @Override
            public int compare(Note o1, Note o2) {
                return o1.getDate().getTime() > o2.getDate().getTime() ? -1 : 0;
            }
        });
        notifyDataSetChanged();
    }

    /**
     * This method help in adding a note to the list
     *
     * @param note object to be added
     */
    public void add(Note note) {
        if (notes == null) {
            notes = new ArrayList<>();
        }
        notes.add(0, note);
        notifyItemInserted(0);
    }

    /**
     * This method removes the item at the position
     *
     * @param position from where item to be removed
     */
    public void removeItem(int position) {
        notes.remove(position);
        notifyItemRemoved(position);
    }
}
