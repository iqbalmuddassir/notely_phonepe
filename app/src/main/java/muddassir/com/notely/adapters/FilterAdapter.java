package muddassir.com.notely.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import muddassir.com.notely.R;
import muddassir.com.notely.models.Filter;
import muddassir.com.notely.viewHolders.FilterHeaderVH;
import muddassir.com.notely.viewHolders.FilterItemClickCallback;
import muddassir.com.notely.viewHolders.FilterItemVH;

/**
 * @author muddassir on 11/18/17.
 */

public class FilterAdapter extends RecyclerView.Adapter {
    private List<Filter> filters;
    private Context context;
    private List<Filter> selectionList;

    public FilterAdapter(List<Filter> filters, Context context) {
        this.filters = filters;
        this.context = context;
        this.selectionList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder;
        View view = View.inflate(context, R.layout.filter_item_layout, null);
        if (viewType == Filter.FILTER_TYPE_HEADER) {
            holder = new FilterHeaderVH(view);
        } else {
            holder = new FilterItemVH(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FilterHeaderVH) {
            ((FilterHeaderVH) holder).fill(filters.get(position));
            ((FilterHeaderVH) holder).setItemClickCallback(new FilterItemClickCallback() {
                @Override
                public void onFilterItemClick(int position) {
                    for (Filter filter : selectionList) {
                        filter.setSelected(false);
                    }
                    selectionList.clear();
                    notifyDataSetChanged();
                }
            });
        } else if (holder instanceof FilterItemVH) {
            ((FilterItemVH) holder).fill(filters.get(position));
            ((FilterItemVH) holder).setItemClickCallback(new FilterItemClickCallback() {
                @Override
                public void onFilterItemClick(int position) {
                    Filter filter = filters.get(position);
                    if (filter.isSelected()) {
                        selectionList.remove(filter);
                        filter.setSelected(false);
                    } else {
                        selectionList.add(filter);
                        filter.setSelected(true);
                    }
                    notifyItemChanged(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return filters == null ? 0 : filters.size();
    }

    @Override
    public int getItemViewType(int position) {
        return filters.get(position).getType();
    }

    /**
     * To know if all filters are selected
     *
     * @return true if selection size is one less than total list size, else false
     */
    public boolean isAllSelected() {
        return (selectionList.size() + 1) == getItemCount();
    }

    /**
     * To know if no selection is being made by user
     *
     * @return true if selection list is empty
     */
    public boolean isNoneSelected() {
        return selectionList.size() == 0;
    }

    /**
     * This method return the selected filter list
     *
     * @return selection list
     */
    public List<Filter> getSelectionList() {
        return selectionList;
    }
}
