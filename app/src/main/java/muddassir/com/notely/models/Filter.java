package muddassir.com.notely.models;

/**
 * @author muddassir on 11/18/17.
 *         This model is to be used as Filter item
 */

public class Filter {
    public static final int FILTER_TYPE_HEADER = 0;
    public static final int FILTER_TYPE_ITEM = 1;

    private String name;
    private int type;
    private boolean isSelected;

    public Filter(String name, int type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
