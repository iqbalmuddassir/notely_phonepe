package muddassir.com.notely.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import java.io.Serializable;
import java.util.Date;

import muddassir.com.notely.room.DateConverter;

/**
 * @author muddassir on 11/17/17.
 *         This model is to be used to represent a note object.
 *         It also represent and hold the information of a note in the database
 *         maintained by Room android APIs
 */

@Entity(tableName = "notes")
@TypeConverters(DateConverter.class)
public class Note implements Serializable {

    static final long serialVersionUID = 1L;
    @Ignore
    public boolean isChanged;
    @Ignore
    public int position;
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "n_id")
    private int mId;
    @ColumnInfo(name = "title")
    private String mTitle;
    @ColumnInfo(name = "content")
    private String mContent;
    @ColumnInfo(name = "last_update")
    private Date mDate;
    @ColumnInfo(name = "starred")
    private boolean isStarred;
    @ColumnInfo(name = "favourite")
    private boolean isFavourite;

    public Note(String title, String content, Date date) {
        this.mTitle = title;
        this.mContent = content;
        this.mDate = date;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String mContent) {
        this.mContent = mContent;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date mDate) {
        this.mDate = mDate;
    }

    public boolean isStarred() {
        return isStarred;
    }

    public void setStarred(boolean starred) {
        isStarred = starred;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    @Override
    public String toString() {
        return "{" +
                "\n\tmId = " + mId +
                "\n\tmTitle = " + mTitle +
                "\n\tmContent = " + mContent +
                "\n\tisChanged = " + isChanged +
                "\n\tisStarred = " + isStarred +
                "\n\tisFavourite = " + isFavourite +
                "\n\tmDate = " + mDate +
                "\n}\n";
    }
}
