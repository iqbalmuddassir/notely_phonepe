package muddassir.com.notely.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import muddassir.com.notely.models.Note;
import muddassir.com.notely.room.NoteDao;

/**
 * @author muddassir on 11/17/17.
 */

@Database(entities = {Note.class}, version = 1, exportSchema = false)
public abstract class NoteDatabase extends RoomDatabase {

    static final String DB = "note_db";
    private static NoteDatabase INSTANCE;

    public static NoteDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), NoteDatabase.class, DB)
                            .build();
        }
        return INSTANCE;
    }

    public abstract NoteDao noteDao();

}