package muddassir.com.notely.db;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;

import muddassir.com.notely.models.Note;

/**
 * @author muddassir on 11/17/17.
 */

public class DatabaseHelper {
    // Database instruction types
    private static final int INSTRUCTION_TYPE_FETCH_ALL = 0;
    private static final int INSTRUCTION_TYPE_INSERT = 1;
    private static final int INSTRUCTION_TYPE_UPDATE = 2;
    private static final int INSTRUCTION_TYPE_DELETE = 3;
    private static final int INSTRUCTION_TYPE_FETCH_STARRED = 4;
    private static final int INSTRUCTION_TYPE_FETCH_FAVOURITE = 5;
    private static final int INSTRUCTION_TYPE_FETCH_STARRED_FAVOURITE = 6;

    // static instance to make it singleton
    private static DatabaseHelper INSTANCE;

    // Database instance
    private NoteDatabase db;

    private DatabaseHelper(Context context) {
        if (context == null) throw new IllegalArgumentException("Context cannot be null");
        db = Room.databaseBuilder(context.getApplicationContext(), NoteDatabase.class, NoteDatabase.DB)
                .build();
    }

    public static DatabaseHelper getInstance(Context context) {
        synchronized (DatabaseHelper.class) {
            if (INSTANCE == null) {
                INSTANCE = new DatabaseHelper(context);
            }
        }
        return INSTANCE;
    }

    // Fetch all the notes from database
    public void fetchAllNotes(DatabaseListener listener) {
        new MyAsync(db, listener).executeOnExecutor(MyAsync.THREAD_POOL_EXECUTOR, INSTRUCTION_TYPE_FETCH_ALL);
    }

    // insert the note to database
    public void insertNote(Note note, DatabaseListener listener) {
        new MyAsync(db, listener).executeOnExecutor(MyAsync.THREAD_POOL_EXECUTOR, INSTRUCTION_TYPE_INSERT, note);
    }

    // update the note in the database
    public void updateNote(Note note, DatabaseListener listener) {
        new MyAsync(db, listener).executeOnExecutor(MyAsync.THREAD_POOL_EXECUTOR, INSTRUCTION_TYPE_UPDATE, note);
    }

    // delete the note from database
    public void deleteNote(Note note, DatabaseListener listener) {
        new MyAsync(db, listener).executeOnExecutor(MyAsync.THREAD_POOL_EXECUTOR, INSTRUCTION_TYPE_DELETE, note);
    }

    // fetch all the starred notes marked by user
    public void fetchStarredNotes(DatabaseListener listener) {
        new MyAsync(db, listener).executeOnExecutor(MyAsync.THREAD_POOL_EXECUTOR, INSTRUCTION_TYPE_FETCH_STARRED);
    }

    // fetch all the favorite notes marked by user
    public void fetchFavouriteNotes(DatabaseListener listener) {
        new MyAsync(db, listener).executeOnExecutor(MyAsync.THREAD_POOL_EXECUTOR, INSTRUCTION_TYPE_FETCH_FAVOURITE);
    }

    // fetch all the notes that are either marked as star or favorite
    public void fetchStarredAndFavouriteNotes(DatabaseListener listener) {
        new MyAsync(db, listener).executeOnExecutor(MyAsync.THREAD_POOL_EXECUTOR, INSTRUCTION_TYPE_FETCH_STARRED_FAVOURITE);
    }

    public interface DatabaseListener {
        void onSuccess(Object... data);

        void onFailure(String failureMessage);
    }

    /**
     * This is the async task that is used to make the async request in the background to
     * interact with the database to complete the request based on instruction type
     */
    static class MyAsync extends AsyncTask<Object, Void, Object> {
        private int instructionType = -1;
        private NoteDatabase db;
        private DatabaseListener listener;

        MyAsync(NoteDatabase db, DatabaseListener listener) {
            this.db = db;
            this.listener = listener;
        }

        @Override
        protected Object doInBackground(Object... data) {
            instructionType = (int) data[0];
            if (instructionType == INSTRUCTION_TYPE_FETCH_ALL) {
                return db.noteDao().getNotes();
            } else if (instructionType == INSTRUCTION_TYPE_INSERT) {
                Note note = (Note) data[1];
                db.noteDao().addNote(note);
                return note;
            } else if (instructionType == INSTRUCTION_TYPE_UPDATE) {
                Note note = (Note) data[1];
                db.noteDao().updateNote(note);
                return note;
            } else if (instructionType == INSTRUCTION_TYPE_DELETE) {
                Note note = (Note) data[1];
                db.noteDao().deleteNote(note);
                return note;
            } else if (instructionType == INSTRUCTION_TYPE_FETCH_STARRED) {
                return db.noteDao().getStarredNotes();
            } else if (instructionType == INSTRUCTION_TYPE_FETCH_FAVOURITE) {
                return db.noteDao().getFavoriteNotes();
            } else if (instructionType == INSTRUCTION_TYPE_FETCH_STARRED_FAVOURITE) {
                return db.noteDao().getFavoriteAndStarredNotes();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            super.onPostExecute(result);
            if (listener != null) {
                listener.onSuccess(result);
            }
        }
    }
}
