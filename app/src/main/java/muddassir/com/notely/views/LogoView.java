package muddassir.com.notely.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

import muddassir.com.notely.R;

/**
 * @author muddassir on 11/17/17.
 *         This is the view representing logo of the app
 */

public class LogoView extends ConstraintLayout {

    public LogoView(Context context) {
        this(context, null);
    }

    public LogoView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LogoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) {
            // inflate the view layout
            inflater.inflate(R.layout.logo_layout, this, true);

            // extract the children
            final View circleOne = findViewById(R.id.logo_circle_1);
            final View circleTwo = findViewById(R.id.logo_circle_2);
            final View circleThree = findViewById(R.id.logo_circle_3);
            final View line = findViewById(R.id.logo_line);

            // set circular backgrounds
            circleOne.setBackground(getCircularDrawable(1));
            circleTwo.setBackground(getCircularDrawable(2));
            circleThree.setBackground(getCircularDrawable(3));

            // Apply scale animation to the bottom line
            ScaleAnimation animation = new ScaleAnimation(
                    0, 1,
                    1, 1,
                    ScaleAnimation.RELATIVE_TO_SELF, 0f,
                    ScaleAnimation.RELATIVE_TO_SELF, 0f
            );
            animation.setDuration(1000);
            line.startAnimation(animation);

            // Build translation animation and apply to the dots
            final float toYTranslate = 0f;
            final float fromYTranslate = -0.8f;
            TranslateAnimation circleOneAnimation = new TranslateAnimation(
                    TranslateAnimation.RELATIVE_TO_SELF, 0,
                    TranslateAnimation.RELATIVE_TO_SELF, 0,
                    TranslateAnimation.RELATIVE_TO_SELF, fromYTranslate,
                    TranslateAnimation.RELATIVE_TO_SELF, toYTranslate
            );
            final int translateDuration = 600;
            circleOneAnimation.setDuration(translateDuration);
            circleOneAnimation.setRepeatCount(TranslateAnimation.INFINITE);
            circleOneAnimation.setRepeatMode(TranslateAnimation.REVERSE);
            circleOne.startAnimation(circleOneAnimation);

            postDelayed(new Runnable() {
                @Override
                public void run() {
                    TranslateAnimation circleTwoAnimation = new TranslateAnimation(
                            TranslateAnimation.RELATIVE_TO_SELF, 0,
                            TranslateAnimation.RELATIVE_TO_SELF, 0,
                            TranslateAnimation.RELATIVE_TO_SELF, fromYTranslate,
                            TranslateAnimation.RELATIVE_TO_SELF, toYTranslate
                    );
                    circleTwoAnimation.setDuration(translateDuration);
                    circleTwoAnimation.setRepeatCount(TranslateAnimation.INFINITE);
                    circleTwoAnimation.setRepeatMode(TranslateAnimation.REVERSE);
                    circleTwo.startAnimation(circleTwoAnimation);
                }
            }, translateDuration / 3);

            postDelayed(new Runnable() {
                @Override
                public void run() {
                    TranslateAnimation circleThreeAnimation = new TranslateAnimation(
                            TranslateAnimation.RELATIVE_TO_SELF, 0,
                            TranslateAnimation.RELATIVE_TO_SELF, 0,
                            TranslateAnimation.RELATIVE_TO_SELF, fromYTranslate,
                            TranslateAnimation.RELATIVE_TO_SELF, toYTranslate
                    );
                    circleThreeAnimation.setDuration(translateDuration);
                    circleThreeAnimation.setRepeatCount(TranslateAnimation.INFINITE);
                    circleThreeAnimation.setRepeatMode(TranslateAnimation.REVERSE);
                    circleThree.startAnimation(circleThreeAnimation);
                }
            }, (translateDuration * 2) / 3);
        }
    }

    /**
     * this method returns the drawable for the dots based on position
     *
     * @param position of the circle dot
     * @return gradient drawable
     */
    private Drawable getCircularDrawable(int position) {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setShape(GradientDrawable.OVAL);
        if (position == 1) {
            drawable.setColor(Color.rgb(0, 255, 0));
        } else if (position == 2) {
            drawable.setColor(Color.rgb(100, 100, 50));
        } else {
            drawable.setColor(Color.rgb(255, 0, 0));
        }
        return drawable;
    }
}
