package muddassir.com.notely.views;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import muddassir.com.notely.R;
import muddassir.com.notely.models.Note;
import muddassir.com.notely.utils.Utils;

/**
 * @author muddassir on 11/17/17.
 */

public class NoteItemView extends ConstraintLayout implements View.OnClickListener {
    private TextView title, content, date;
    private ImageView favourite, star;
    private NoteItemClick noteItemClick;
    private int position;

    public NoteItemView(Context context) {
        this(context, null);
    }

    public NoteItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NoteItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (inflater != null) {
            inflater.inflate(R.layout.note_item_layout, this, true);
            title = findViewById(R.id.note_title);
            content = findViewById(R.id.note_content);
            date = findViewById(R.id.note_date);
            favourite = findViewById(R.id.note_heart);
            star = findViewById(R.id.note_star);
            favourite.setOnClickListener(this);
            star.setOnClickListener(this);
        }
    }

    public void fill(Note note) {
        title.setText(note.getTitle());
        content.setText(note.getContent());
        date.setText(Utils.getFormattedTime(note.getDate()));

        if (note.isFavourite()) {
            setFilledHeart();
        } else {
            setUnFilledHeart();
        }

        if (note.isStarred()) {
            setFilledStar();
        } else {
            setUnFilledStar();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.note_star:
                if (noteItemClick != null) {
                    noteItemClick.onStarClick(position);
                }
                break;
            case R.id.note_heart:
                if (noteItemClick != null) {
                    noteItemClick.onHeartClick(position);
                }
                break;
        }
    }

    public void setFilledStar() {
        star.setImageResource(R.drawable.ic_action_star);
    }

    public void setFilledHeart() {
        favourite.setImageResource(R.drawable.ic_action_favorite);
    }

    public void setUnFilledStar() {
        star.setImageResource(R.drawable.ic_action_star_unfilled);
    }

    public void setUnFilledHeart() {
        favourite.setImageResource(R.drawable.ic_action_favorite_unfilled);
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setNoteItemClick(NoteItemClick noteItemClick) {
        this.noteItemClick = noteItemClick;
    }

    public interface NoteItemClick {
        void onStarClick(int position);

        void onHeartClick(int position);
    }
}
