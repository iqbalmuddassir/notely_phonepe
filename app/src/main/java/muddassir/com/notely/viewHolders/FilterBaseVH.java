package muddassir.com.notely.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import muddassir.com.notely.R;
import muddassir.com.notely.models.Filter;

/**
 * @author muddassir on 11/18/17.
 */

public abstract class FilterBaseVH extends RecyclerView.ViewHolder {

    TextView filterName;
    ImageView filterAction;
    FilterItemClickCallback itemClickCallback;

    FilterBaseVH(View itemView) {
        super(itemView);

        filterName = itemView.findViewById(R.id.filter_name);
        filterAction = itemView.findViewById(R.id.filter_action);
    }

    abstract public void fill(Filter filter);

    public void setItemClickCallback(FilterItemClickCallback itemClickCallback) {
        this.itemClickCallback = itemClickCallback;
    }
}
