package muddassir.com.notely.viewHolders;

import android.view.View;

import muddassir.com.notely.R;
import muddassir.com.notely.models.Filter;

/**
 * @author muddassir on 11/18/17.
 */

public class FilterItemVH extends FilterBaseVH {
    public FilterItemVH(View itemView) {
        super(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemClickCallback != null) {
                    itemClickCallback.onFilterItemClick(getAdapterPosition());
                }
            }
        });
    }

    @Override
    public void fill(Filter filter) {
        filterName.setText(filter.getName());
        filterAction.setImageResource(R.drawable.ic_action_delete);

        // Update the value of text and icon based on selection
        if (filter.isSelected()) {
            filterAction.setImageResource(R.drawable.ic_action_tick_selected);
            filterName.setTextColor(itemView.getResources().getColor(R.color.itemSelected));
        } else {
            filterAction.setImageResource(R.drawable.ic_action_tick);
            filterName.setTextColor(itemView.getResources().getColor(R.color.white));
        }
    }
}
