package muddassir.com.notely.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import muddassir.com.notely.db.DatabaseHelper;
import muddassir.com.notely.models.Note;
import muddassir.com.notely.views.NoteItemView;

/**
 * @author muddassir on 11/17/17.
 */

public class NoteItemVH extends RecyclerView.ViewHolder {

    private NoteItemView noteItemView;
    private OnItemClickListener onItemClickListener;
    private Note note;

    public NoteItemVH(View itemView) {
        super(itemView);
        noteItemView = (NoteItemView) itemView;
        noteItemView.setPosition(getAdapterPosition());
        noteItemView.setNoteItemClick(new NoteItemView.NoteItemClick() {
            @Override
            public void onStarClick(int position) {
                // Update starred value of view as well as database
                if (note.isStarred()) {
                    note.setStarred(false);
                    noteItemView.setUnFilledStar();
                } else {
                    note.setStarred(true);
                    noteItemView.setFilledStar();
                }
                DatabaseHelper.getInstance(noteItemView.getContext()).updateNote(note, null);
            }

            @Override
            public void onHeartClick(int position) {
                // Update favorite value of view as well as database
                if (note.isFavourite()) {
                    note.setFavourite(false);
                    noteItemView.setUnFilledHeart();
                } else {
                    note.setFavourite(true);
                    noteItemView.setFilledHeart();
                }
                DatabaseHelper.getInstance(noteItemView.getContext()).updateNote(note, null);
            }
        });
        noteItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // send item click callback
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(getAdapterPosition());
                }
            }
        });
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setNote(Note note) {
        this.note = note;
        noteItemView.fill(note);
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
