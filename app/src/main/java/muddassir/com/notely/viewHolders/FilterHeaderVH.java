package muddassir.com.notely.viewHolders;

import android.view.View;

import muddassir.com.notely.R;
import muddassir.com.notely.models.Filter;

/**
 * @author muddassir on 11/18/17.
 */

public class FilterHeaderVH extends FilterBaseVH {
    public FilterHeaderVH(View itemView) {
        super(itemView);

        filterAction.setImageResource(R.drawable.ic_action_cross);
        filterAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemClickCallback != null) {
                    itemClickCallback.onFilterItemClick(getAdapterPosition());
                }
            }
        });
    }

    @Override
    public void fill(Filter filter) {
        filterName.setText(filter.getName());
    }
}
