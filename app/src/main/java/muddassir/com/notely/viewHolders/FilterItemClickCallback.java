package muddassir.com.notely.viewHolders;

/**
 * @author muddassir on 11/18/17.
 */

public interface FilterItemClickCallback {
    void onFilterItemClick(int position);
}
