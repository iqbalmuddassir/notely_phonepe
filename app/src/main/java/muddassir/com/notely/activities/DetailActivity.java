package muddassir.com.notely.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Date;

import muddassir.com.notely.R;
import muddassir.com.notely.db.DatabaseHelper;
import muddassir.com.notely.models.Note;
import muddassir.com.notely.utils.Utils;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {

    // intent extra keys
    public static final String EXTRA_NOTE = "extra_note";
    public static final String CREATE_NOTE = "extra_create_note";

    // Views
    EditText etDetailContent, etDetailTitle;
    TextView tvDetailContent, tvDetailTitle, tvDetailDate;
    ViewGroup editorLayout, detailLayout;

    // Fields
    private Note note;
    private boolean isEditing;
    private boolean isNewNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        // Edit layout
        editorLayout = findViewById(R.id.detail_editor_layout);
        etDetailTitle = editorLayout.findViewById(R.id.detail_title);
        etDetailContent = editorLayout.findViewById(R.id.detail_content);

        // Detail layout
        detailLayout = findViewById(R.id.detail_note_layout);
        tvDetailDate = detailLayout.findViewById(R.id.detail_note_date);
        tvDetailTitle = detailLayout.findViewById(R.id.detail_note_title);
        tvDetailContent = detailLayout.findViewById(R.id.detail_note_content);

        // Change the status bar color
        Utils.changeTheme(this, getResources().getColor(R.color.toolbarColor));

        // read the intent
        getNoteFromIntent();
    }

    private void getNoteFromIntent() {
        if (getIntent() != null) {
            isNewNote = getIntent().getBooleanExtra(CREATE_NOTE, false);
            if (isNewNote) { // new note request
                enterIntoEditMode();
            } else { // view note request
                note = (Note) getIntent().getSerializableExtra(EXTRA_NOTE);
                enterIntoDetailMode();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.detail_save:
                saveNote();
                break;
            case R.id.detail_undo:
                reset();
                break;
            case R.id.detail_back:
                onBackPressed();
                break;
            case R.id.detail_edit:
                enterIntoEditMode();
                break;
        }
    }

    // To enter into edit mode in order to edit the note
    private void enterIntoEditMode() {
        isEditing = true;
        editorLayout.setVisibility(View.VISIBLE);
        detailLayout.setVisibility(View.GONE);
        findViewById(R.id.detail_save).setVisibility(View.VISIBLE);
        findViewById(R.id.detail_undo).setVisibility(View.VISIBLE);
        findViewById(R.id.detail_edit).setVisibility(View.GONE);
        if (note != null) {
            etDetailTitle.setText(note.getTitle());
            etDetailContent.setText(note.getContent());
            etDetailContent.requestFocus();
            etDetailContent.setSelection(etDetailContent.length());
            Utils.showKeyboard(etDetailContent);
        }
    }

    // To enter into detail view mode in order to view the note
    private void enterIntoDetailMode() {
        isEditing = false;
        editorLayout.setVisibility(View.GONE);
        detailLayout.setVisibility(View.VISIBLE);
        findViewById(R.id.detail_save).setVisibility(View.GONE);
        findViewById(R.id.detail_undo).setVisibility(View.GONE);
        findViewById(R.id.detail_edit).setVisibility(View.VISIBLE);
        if (note != null) {
            tvDetailTitle.setText(note.getTitle());
            tvDetailContent.setText(note.getContent());
            tvDetailDate.setText(Utils.lastUpdatedFormatTime(note.getDate()));
        }
    }

    // this method is used to undo/reset to previous state of the note
    private void reset() {
        if (note != null) {
            etDetailTitle.setText(note.getTitle());
            etDetailContent.setText(note.getContent());
            etDetailContent.requestFocus();
            etDetailContent.setSelection(etDetailContent.length());
        }
    }

    // This method save the note if any
    private void saveNote() {
        // setup database listener
        DatabaseHelper.DatabaseListener listener = new DatabaseHelper.DatabaseListener() {
            @Override
            public void onSuccess(Object... data) {
                if (isNewNote) {
                    Note note = (Note) data[0];
                    Intent intent = new Intent();
                    intent.putExtra(EXTRA_NOTE, note);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    enterIntoDetailMode();
                }
            }

            @Override
            public void onFailure(String failureMessage) {

            }
        };

        if (note == null) { // create new note if value exist
            if (!etDetailTitle.getText().toString().trim().isEmpty()) {
                // create new note only if user have entered title
                note = new Note(etDetailTitle.getText().toString().trim()
                        , etDetailContent.getText().toString().trim()
                        , new Date());
                DatabaseHelper.getInstance(this).insertNote(note, listener);
            }
        } else {
            // update the existing note
            note.setTitle(etDetailTitle.getText().toString());
            note.setContent(etDetailContent.getText().toString());
            note.setDate(new Date());
            note.isChanged = true;
            DatabaseHelper.getInstance(this).updateNote(note, listener);
        }
        Utils.hideKeyboard(this);
    }

    @Override
    public void onBackPressed() {
        Utils.hideKeyboard(this);
        if (!isNewNote && isEditing) {
            enterIntoDetailMode();
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void finish() {
        if (note != null && note.isChanged) {
            Intent intent = new Intent();
            intent.putExtra(EXTRA_NOTE, note);
            setResult(RESULT_OK, intent);
        }
        super.finish();
    }
}
