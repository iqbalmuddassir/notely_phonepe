package muddassir.com.notely.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import muddassir.com.notely.R;
import muddassir.com.notely.adapters.FilterAdapter;
import muddassir.com.notely.adapters.NoteAdapter;
import muddassir.com.notely.db.DatabaseHelper;
import muddassir.com.notely.models.Filter;
import muddassir.com.notely.models.Note;
import muddassir.com.notely.utils.Utils;
import muddassir.com.notely.viewHolders.NoteItemVH;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    // Filter types
    public static final String FILTER = "FILTER";
    public static final String FAVOURITE = "Favourite";
    public static final String STARRED = "Starred";

    // Request codes
    private static final int ADD_NOTE = 0x01;
    private static final int VIEW_NOTE = 0x02;

    // Views
    private DrawerLayout drawer;
    private RecyclerView noteList, filterList;

    // Adapters
    private FilterAdapter filterAdapter;
    private NoteAdapter noteAdapter;

    // Other fields
    private Paint paint = new Paint(); // paint to draw delete menu
    private DatabaseHelper.DatabaseListener databaseListener; // listener for handling database

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // change the status bar color
        Utils.changeTheme(this, getResources().getColor(R.color.toolbarColor));

        // extract the views
        Button filterApplyBtn = findViewById(R.id.home_apply_btn);
        drawer = findViewById(R.id.drawer_layout);
        noteList = findViewById(R.id.home_notes);
        filterList = findViewById(R.id.home_filter_list);

        // set apply filter button background drawable
        GradientDrawable drawable = new GradientDrawable();
        drawable.setStroke(10, Color.WHITE);
        filterApplyBtn.setBackground(drawable);

        // setup filters and notes list
        setUpFilter();
        setUpNotes();
    }

    // **** Note related methods **** //

    /**
     * This method setup the note recycler view, its adapter, item swipe listener and
     * database listener. It also fill the list initially if any.
     */
    private void setUpNotes() {
        // set layout manager
        noteList.setLayoutManager(new LinearLayoutManager(this));

        // initialise and setup note list adapter
        noteAdapter = new NoteAdapter(this);
        noteAdapter.setOnItemClickListener(new NoteItemVH.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Note note = noteAdapter.getItem(position);
                note.position = position;
                sendOpenNoteIntent(note);
            }
        });

        // setup item swipe using item touch helper
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(
                0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT) {
                    deleteNote(position);
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if (dX < 0) {
                        paint.setColor(getResources().getColor(R.color.deleteColor));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, paint);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_delete);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, paint);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(noteList);

        // set adapter to note list
        noteList.setAdapter(noteAdapter);

        // add divider
        noteList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        // initialise database listener
        databaseListener = new DatabaseHelper.DatabaseListener() {
            @Override
            public void onSuccess(Object... data) {
                if (data != null && data.length > 0) {
                    List<Note> notes = (List<Note>) data[0];
                    Log.d("AllNotes", notes.toString());
                    noteAdapter.setNotes(notes);
                }
            }

            @Override
            public void onFailure(String failureMessage) {

            }
        };

        // fetch all notes
        fetchNotes(databaseListener, 0);
    }

    /**
     * Fetch the notes based on the type of request
     *
     * @param listener listener to handle the database request
     * @param type     type of request
     */
    private void fetchNotes(DatabaseHelper.DatabaseListener listener, int type) {
        if (type == 0) { // fetch all notes
            DatabaseHelper.getInstance(this).fetchAllNotes(listener);
        } else if (type == 1) { // fetch starred notes
            DatabaseHelper.getInstance(this).fetchStarredNotes(listener);
        } else if (type == 2) { // fetch favorite notes
            DatabaseHelper.getInstance(this).fetchFavouriteNotes(listener);
        } else if (type == 3) { // fetch starred and favorite notes
            DatabaseHelper.getInstance(this).fetchStarredAndFavouriteNotes(listener);
        }
    }

    // send the add note intent to detail activity
    private void sendAddNoteIntent() {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(DetailActivity.CREATE_NOTE, true);
        startActivityForResult(intent, ADD_NOTE);
    }

    // send the view note intent to the detail activity
    private void sendOpenNoteIntent(Note note) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(DetailActivity.CREATE_NOTE, false);
        intent.putExtra(DetailActivity.EXTRA_NOTE, note);
        startActivityForResult(intent, VIEW_NOTE);
    }

    /**
     * Delete note from database
     *
     * @param position of the note item
     */
    private void deleteNote(final int position) {
        Note note = noteAdapter.getItem(position);
        DatabaseHelper.getInstance(this).deleteNote(note, new DatabaseHelper.DatabaseListener() {
            @Override
            public void onSuccess(Object... data) {
                noteAdapter.removeItem(position);
            }

            @Override
            public void onFailure(String failureMessage) {

            }
        });
    }

    // **** Filter related methods **** //

    /**
     * This method setup the filter recycler view, drawer, its adapter and its data.
     * It also fill the list initially.
     */
    private void setUpFilter() {
        // add list item
        List<Filter> filterList = new ArrayList<>();
        filterList.add(new Filter(FILTER, Filter.FILTER_TYPE_HEADER));
        filterList.add(new Filter(FAVOURITE, Filter.FILTER_TYPE_ITEM));
        filterList.add(new Filter(STARRED, Filter.FILTER_TYPE_ITEM));
        filterAdapter = new FilterAdapter(filterList, this);
        this.filterList.setLayoutManager(new LinearLayoutManager(this));
        this.filterList.setAdapter(filterAdapter);

        // Drawer toggle setup
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                float moveFactor = (HomeActivity.this.filterList.getWidth() * slideOffset);
                noteList.setTranslationX(-moveFactor);
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        closeFilterDrawer();
    }

    // this method applies the filter selected by the user
    private void applyFilter() {
        if (filterAdapter.isAllSelected()) {
            fetchNotes(databaseListener, 3);
        } else if (filterAdapter.isNoneSelected()) {
            fetchNotes(databaseListener, 0);
        } else {
            String name = filterAdapter.getSelectionList().get(0).getName();
            if (FAVOURITE.equals(name)) {
                fetchNotes(databaseListener, 2);
            } else {
                fetchNotes(databaseListener, 1);
            }
        }

        closeFilterDrawer();
    }

    // **** UI action methods **** //

    private void closeFilterDrawer() {
        drawer.closeDrawer(GravityCompat.END);
    }

    private void openFilterDrawer() {
        drawer.openDrawer(GravityCompat.END);
    }

    private boolean isFilterDrawerOpen() {
        return drawer.isDrawerOpen(GravityCompat.END);
    }

    private void scrollToBottom() {
        noteList.smoothScrollToPosition(noteAdapter.getItemCount());
    }

    private void scrollToTop() {
        noteList.smoothScrollToPosition(0);
    }

    @Override
    public void onBackPressed() {
        if (isFilterDrawerOpen()) {
            closeFilterDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.home_add:
                sendAddNoteIntent();
                break;
            case R.id.home_filter:
                openFilterDrawer();
                break;
            case R.id.home_title:
                scrollToTop();
                break;
            case R.id.home_apply_btn:
                applyFilter();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case ADD_NOTE: {
                    Note note = (Note) data.getSerializableExtra(DetailActivity.EXTRA_NOTE);
                    if (note != null) {
                        noteAdapter.add(note);
                        scrollToTop();
                    }
                    break;
                }
                case VIEW_NOTE: {
                    Note note = (Note) data.getSerializableExtra(DetailActivity.EXTRA_NOTE);
                    if (note.isChanged) {
                        noteAdapter.set(note.position, note);
                        scrollToTop();
                    }
                    break;
                }
            }
        }
    }
}
