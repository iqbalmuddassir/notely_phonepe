package muddassir.com.notely.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import muddassir.com.notely.R;
import muddassir.com.notely.utils.Utils;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // change status bar color
        Utils.changeTheme(this, getResources().getColor(R.color.toolbarColor));

        // Dummy timer for splash
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // start home activity
                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        }, 2000);
    }
}
