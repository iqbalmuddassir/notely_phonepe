package muddassir.com.notely.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;
import android.arch.persistence.room.Update;

import java.util.List;

import muddassir.com.notely.models.Note;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * @author muddassir on 11/17/17.
 *         Dao for {@link Note} to be interface it with {@link muddassir.com.notely.db.NoteDatabase}
 */

@Dao
@TypeConverters(DateConverter.class)
public interface NoteDao {

    @Query("SELECT * FROM notes")
    List<Note> getNotes();

    @Query("select * from notes where n_id = :id")
    Note getItemById(int id);

    @Insert(onConflict = REPLACE)
    void addNote(Note note);

    @Update
    void updateNote(Note note);

    @Delete
    void deleteNote(Note note);

    @Query("SELECT * FROM notes WHERE starred")
    List<Note> getStarredNotes();

    @Query("SELECT * FROM notes WHERE favourite")
    List<Note> getFavoriteNotes();

    @Query("SELECT * FROM notes WHERE starred OR favourite")
    List<Note> getFavoriteAndStarredNotes();
}
