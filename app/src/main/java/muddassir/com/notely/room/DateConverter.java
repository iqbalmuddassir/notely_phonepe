package muddassir.com.notely.room;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

/**
 * @author muddassir on 11/17/17.
 *         This class is used as data converter for {@link Date} object
 */

public class DateConverter {

    @TypeConverter
    public static Date toDate(Long timestamp) {
        return timestamp == null ? null : new Date(timestamp);
    }

    @TypeConverter
    public static Long toTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }
}
